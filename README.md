# Chromin
> **Please Note:**

>Chromin is in **early** developement, and is still being planned out. There may be **frequent** changes to the syntax.

## What is Chromin?
Chromin is a tool to make console logs more readable with custom colors, tags, and formats.

## Installation
Installing chromin is just like any other package, open a terminal in your project directory and execute:
```bash
$ npm install chromin
```

## Using Chromin
Chromin has a few basic presets. They are ```logError()```, ```logStatus()```, and ```logWarning()```. To use one of these presets, simply call the function and provide a string to be logged.
```JavaScript
chromin.logWarning('User ' + user.id + ' has connected with an invalid device id');
```
---
For basic use, chromin uses hints to define different colors:
```JavaScript
chromin.hint(<section>, <type>, <color>);
```
For example, if we wanted our log to have a tag with cyan text, and a body with magenta text we would use:
```JavaScript
chromin.hint(chromin.section.Tag, chromin.type.Foreground, chromin.colors.cyan);
chromin.hint(chromin.section.Body, chromin.type.Foreground, chromin.colors.magenta);
```
---
To manually change the delimiters, use tag's ```setDelimiters()``` function:
```JavaScript
chromin.tag.setDelimiters('<', '>');
```
And to set the text for the tag, use tag's ```setText()``` function:
```JavaScript
chromin.tag.setText('NewPlayer');
```
And to change the tag separator, use tag's ```setSeparator()``` function:
```JavaScript
chromin.tag.setSeparator(' - ');
```

---
Using hints can be tedious. If you would like to define presets for your program, chromin supports the use of json objects to set the current style. To do this, use the function ```chromin.setConfig()``` and provide a valid chromin json object. 

An example of a valid chromin json object:
``` JavaScript
var newPlayerConfig = 
{
    "tag": {
        "delimiters": {
           "beginning": "[",
            "end": "]"
        },
        "colors": {
            "foreground": "colors.cyan",
            "background": "colors.black"
        },
        "text": "New Player"
    },

    "body": {
        "colors": {
            "foreground": "colors.white",
            "background": "colors.black"
        }
    },

    "separator": {
        "colors": {
            "foreground": "colors.white",
            "background": "colors.black"
        },
        "text": " -=+=- "
    }
};
``` 
A more convenient way to use objects is to load them from a json file:
```JavaScript
var newPlayerConfig = JSON.parse(fs.readFileSync('new-player.json', 'utf8'));
```
Then load the config with chromin:
```JavaScript
chromin.loadConfig(newPlayerConfig);
```
Now use ```logCustom()```:
```JavaScript
chromin.logCustom('Player ' + player.id + ' has joined for the first time!');
```

---

**Contributing:**
Take care to follow the origional formatting, and provide useful and informative comments. 
