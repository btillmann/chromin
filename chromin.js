
// Different color hint types
var Background = 'bg';
var Foreground = 'fg';

// Different color hint parts
var Tag = 'tag';
var Body = 'body';

// The color escape codes
var colors = {
    black: 'black',
    white: 'white',
    red: 'red',
    green: 'green',
    yellow: 'yellow',
    blue: 'blue',
    magenta: 'magenta',
    cyan: 'cyan',
};

var bgColors = {
    black: '\x1b[40m',
    white: '\x1b[47m',
    red: '\x1b[41m',
    green: '\x1b[42m',
    yellow: '\x1b[43m',
    blue: '\x1b[44m',
    magenta: '\x1b[45m',
    cyan: '\x1b[46m'
};

var fgColors = {
    black: '\x1b[30m',
    white: '\x1b[37m',
    red: '\x1b[31m',
    green: '\x1b[32m',
    yellow: '\x1b[33m',
    blue: '\x1b[34m',
    magenta: '\x1b[35m',
    cyan: '\x1b[36m'
};

function getProperColor(color, type) {
    switch (color) {
        case colors.black:
            if (type == Background)
                return bgColors.black;
            return fgColors.black;

        case colors.white:
            if (type == Background)
                return bgColors.white;
            return fgColors.white;

        case colors.red:
            if (type == Background)
                return bgColors.red;
            return fgColors.red;

        case colors.green:
            if (type == Background)
                return bgColors.green;
            return fgColors.green;

        case colors.yellow:
            if (type == Background)
                return bgColors.yellow;
            return fgColors.yellow;

        case colors.blue:
            if (type == Background)
                return bgColors.blue;
            return fgColors.blue;

        case colors.magenta:
            if (type == Background)
                return bgColors.magenta;
            return fgColors.magenta;

        case colors.cyan:
            if (type == Background)
                return bgColors.cyan;
            return fgColors.cyan;

        default:
            if (type == Background)
                return bgColors.white;
            return fgColors.white;
    }
}

// Set the config from a json object
function setConfig(config){
    tagColor.background = getProperColor(eval(config.tag.colors.background), Background);
    tagColor.foreground = getProperColor(eval(config.tag.colors.foreground), Foreground);
    tagDelimiters = config.tag.delimiters;
    tagText = config.tag.text;

    bodyColor.background = getProperColor(eval(config.body.colors.background), Background);
    bodyColor.foreground = getProperColor(eval(config.body.colors.foreground), Foreground);

    separatorColor.background = getProperColor(eval(config.separator.colors.background), Background);
    separatorColor.foreground = getProperColor(eval(config.separator.colors.foreground), Foreground);
    separatorText = config.separator.text;
}

// Loads the specified json file and sets the config
function loadConfig(path){
    var cfg = JSON.parse(fs.readFileSync(path, 'utf8'));
    setConfig(cfg);
}

// Default tag color values
var tagColor = {
    background: bgColors.black,
    foreground: fgColors.white
};

// Default body color values
var bodyColor = {
    background: bgColors.black,
    foreground: fgColors.white
};

// Color of the separator
var separatorColor = {
    background: bgColors.black,
    foreground: fgColors.white
};

// The default text color
var defaultColor = {
    background: bgColors.black,
    foreground: fgColors.white
};

// The characters used to bound the tag text
var tagDelimiters = {
    beginning: '[',
    end: ']'
};

// A buffer to hold the text
var separatorText = " => ";
var tagText = "";
var bodyText = "";

function hint(part, type, color) {
    if (part == Tag) {
        var color = getProperColor(color, type);

        if (type == Background) {
            tagColor.background = color;
        }
        else if (type == Foreground) {
            tagColor.foreground = color;
        }
        else {
            tagColor.background = bgColors.black;
            tagColor.foreground = fgColors.white;
        }
    }
    else if (part == Body) {
        var color = getProperColor(color, type);

        if (type == Background) {
            bodyColor.background = color;
        }
        else if (type == Foreground) {
            bodyColor.foreground = color;
        }
        else {
            bodyColor.background = bgColors.black;
            bodyColor.foreground = fgColors.white;
        }
    }
    else {
        tagColor.background = bgColors.black;
        tagColor.foreground = fgColors.white;
        bodyColor.background = bgColors.black;
        bodyColor.foreground = fgColors.white;
    }
}

function setTagDelimiters(beginning, end){
    tagDelimiters.beginning = beginning;
    tagDelimiters.end = end;
}

function setSeparator(separator){
    separatorText = separator;
}

function setTagText(text){
    tagText = text;
}

function logError(text) {
    hint(Tag, Foreground, colors.red);
    setTagDelimiters('[', ']');
    setTagText('ERROR');
    logCustom(text);
}

function logWarning(text) {
    hint(Tag, Foreground, colors.yellow);
    setTagDelimiters('[', ']');
    setTagText('WARNING');
    logCustom(text);
}

function logStatus(text) {
    hint(Tag, Foreground, colors.magenta);
    setTagDelimiters('[', ']');
    setTagText('STATUS');
    logCustom(text);
}

// Uses previous color hints instead
function logCustom(message) {
    bodyText = message;

    flush();
}

function log(text) {
    console.log(text);
}

// A private flush log function
function flush() {
    // console.log(tagColor.foreground + tagColor.background + tagTextBuffer + bodyColor.foreground + bodyColor.background + bodyTextBuffer + fgColors.white + bgColors.black);
    console.log(tagDelimiters.beginning 
        + tagColor.background 
        + tagColor.foreground 
        + tagText
        + defaultColor.foreground 
        + defaultColor.background 
        + tagDelimiters.end 
        + separatorColor.background
        + separatorColor.foreground
        + separatorText
        + bodyColor.foreground 
        + bodyColor.background 
        + bodyText
        + defaultColor.foreground 
        + defaultColor.background);
}

module.exports = {
    type: {
        Foreground: Foreground,
        Background: Background
    },

    section: {
        Tag: Tag,
        Body: Body
    },

    tag: {
        setDelimiters: setTagDelimiters,
        setSeparator: setSeparator,
        setText: setTagText
    },

    log: log,
    logCustom: logCustom,
    logError: logError,
    logWarning: logWarning,
    logStatus: logStatus,
    hint: hint,
    colors: colors,
    logCustom: logCustom,
    setConfig: setConfig
};