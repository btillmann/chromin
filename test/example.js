var chromin = require('../chromin.js');
var fs = require('fs');

// Some vars to make life easier
var section = chromin.section;
var colors = chromin.colors;
var type = chromin.type;

// Using the built in log functions
chromin.logError('This is an error');
chromin.logStatus('This is status');
chromin.logWarning('This is a warning');

// Using hints to set colors
chromin.hint(section.Tag, type.Foreground, colors.black);
chromin.hint(section.Tag, type.Background, colors.white);
chromin.hint(section.Body, type.Foreground, colors.white);

// Setting the tag delimiters
chromin.tag.setDelimiters('<', '>');

// Log custom uses previous hints
chromin.tag.setText('CUSTOM')
chromin.logCustom('This is a custom log using color hints');

// Using a custom separator
chromin.tag.setSeparator(' -=- ');
chromin.tag.setText('ANOTHER CUSTOM');
chromin.logCustom('This is a custom log with a custom separator');

// Here we load a json object from a json file, and use it to set the formatting
var newuser_cfg = JSON.parse(fs.readFileSync('test/new-user.json', 'utf8'));
chromin.setConfig(newuser_cfg);
chromin.logCustom('This is a log that used a json object to define the formatting');